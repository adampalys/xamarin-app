﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FormPage : ContentPage
	{
        private Entry _nameEntry;
        private Entry _descriptionEntry;

        public FormPage ()
		{
			InitializeComponent ();
            _nameEntry = this.FindByName<Entry>("NameEntry");
            _descriptionEntry = this.FindByName<Entry>("DescriptionEntry");
        }

        private void OnButtonClicked(object sender, EventArgs e)
        {
            using(SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.DB_PATH))
            {
                conn.CreateTable<Task>();
                var numberOfRows = conn.Insert(new Task()
                {
                    Name = _nameEntry.Text,
                    Description = _descriptionEntry.Text
                });

                if (numberOfRows > 0)
                    DisplayAlert("Success", "New task has been added!", "Ok");
                else
                    DisplayAlert("Failure", "Something went wrong!", "Ok");
            }

        }
    }
}