﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
	{
        private ListView _taskList;

        public MainPage()
		{
			InitializeComponent();
            _taskList = this.FindByName<ListView>("TaskList");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            using(SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.DB_PATH))
            {
                conn.CreateTable<Task>();
                var tasks = conn.Table<Task>().ToList();
                
                _taskList.ItemsSource = tasks;
            }
        }

        private void OnItemActivated(object sender, EventArgs e)
        {
            Navigation.PushAsync(new FormPage());
        }
    }
}
